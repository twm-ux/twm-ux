# README #

File repository and versioning for Total Wine and More UX team.

## Resources ##

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## How do I get set up? ##

* Clone repo
* Smack it up
* Flip it
* Rub it down
* OH NO!

## Contribution best practices ##

* Always pull before beginning work
* Commit regularly at appropriate intervals
* Lean towards granular over monolithic when configuring files to mitigate merge conflicts

## Owners ##

* Jackson Stephens: jstephens@totalwine.com